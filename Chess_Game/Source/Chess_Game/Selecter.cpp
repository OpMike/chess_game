// Michael Opoku 2017

#include "Chess_Game.h"
#include "Selecter.h"
#include "ChessBoard.h"


#define OUT


USelecter::USelecter()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void USelecter::BeginPlay()
{
	UE_LOG(LogTemp, Warning, TEXT("Selecter!"));
	Super::BeginPlay();
	SetupInputComponent();
	SetupChessBoard();
}


void USelecter::SetupInputComponent()
{
	PlayerController = GetWorld()->GetFirstPlayerController();

	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		// Bind input
		InputComponent->BindAction("Select", IE_Pressed, this, &USelecter::Select);
		InputComponent->BindAction("Deselect", IE_Pressed, this, &USelecter::Deselect);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing input component!"), *GetOwner()->GetName());
	}
}

void USelecter::SetupChessBoard()
{
	for (TObjectIterator<UChessBoard> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		Board = *Itr;
		break;
	}

	if (Board  == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Attach board to %s"), *GetOwner()->GetName());
	}
}


void USelecter::Select()
{
	if (PlayerController)
	{
		FHitResult TraceResult(ForceInit);
		PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, OUT TraceResult);
		FVector GridLocation;

		if (TraceResult.GetComponent() != nullptr){
			FVector loc = TraceResult.GetComponent()->GetComponentLocation() / FVector(400.f, 400.f, 1.f);
			GridLocation.X = loc.Y;
			GridLocation.Y = loc.X;
		}
		
		if (GEngine != nullptr) 
		{
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, FString::Printf(TEXT("%i %i"), (int)GridLocation.X, (int)GridLocation.Y));
		}
		
		if (TraceResult.GetActor() != nullptr)
		{
			if (SelectedPiece != TraceResult.GetActor())
			{
				if (SelectedPiece) //if piece is already selected move it
				{

					Piece = SelectedPiece->FindComponentByClass<UChessPiece>();
						

					if (Board->GetWhosTurn() == Piece->GetTeam())
					{					
						Piece->MoveToGrid(GridLocation.X, GridLocation.Y);
					}

					SelectedPiece = nullptr;
					Piece = nullptr;
				}
				else if (TraceResult.GetActor()->FindComponentByClass<UChessPiece>())
				{
					SelectedPiece = TraceResult.GetActor();

				}
			}
		}

	}
	else {
		UE_LOG(LogTemp, Error, TEXT("%s missing player controller!"), *GetOwner()->GetName());
		return;
	}
}


void USelecter::Deselect()
{
	SelectedPiece = nullptr;
	UE_LOG(LogTemp, Warning, TEXT("Deselect."));
}