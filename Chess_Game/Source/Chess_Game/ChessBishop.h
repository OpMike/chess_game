// Michael Opoku 2017

#pragma once

#include "ChessPiece.h"
#include "ChessBishop.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API UChessBishop : public UChessPiece
{
	GENERATED_BODY()

public:	
	UChessBishop();
	virtual void SendPossibleMovesToEnemyKing(int moveStage) override;

protected:
	virtual bool IsValidMove(int x, int y) override;

private:
	virtual void BeginPlay() override;
};
