// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessBoard.h"
#include "ChessPiece.h"
#include "ChessKing.h"

#include "EngineUtils.h" 


UChessBoard::UChessBoard()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessBoard::BeginPlay()
{
	Super::BeginPlay();
}


UChessPiece* UChessBoard::FindPieceAt(int _x, int _y)
{
	int x, y;

	for (TObjectIterator<UChessPiece> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		x = (int)Itr->GetCurrentGridLocation()->X;
		y = (int)Itr->GetCurrentGridLocation()->Y;

		if (x == _x && y == _y)
		{
			return *Itr;
		}
	}
	return nullptr;
}


void UChessBoard::DeletePieceAt(int _x, int _y)
{
	int x, y;

	for (TObjectIterator<UChessPiece> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		x = Itr->GetCurrentGridLocation()->X;
		y = Itr->GetCurrentGridLocation()->Y;

		if (x == _x && y == _y)
		{
			Itr->SetCurrentGridLocation(FIntPoint(-1, -1)); // Workaround as TObjectIterator does not remove the Pieces straight away ,should be solved by world check
			Itr->CallDestroy();
		}
	}
}

int UChessBoard::GetWhosTurn()
{
	return Turn;
}


void UChessBoard::NextTurn()
{
	if (Turn == 1)
	{
		Turn = 2;
	} else if (Turn == 2)  {

		Turn = 1;
	}
}


void UChessBoard::CalculateAllPossibleMoves()
{
	// Reset own kings IsMoveIntoCheck
	for (TObjectIterator<UChessKing> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() == Itr->GetTeam()) 
		{
			Itr->ResetIsMoveIntoCheck();
		}
	}

	// check possible moves for enemy pieces against king position
	for (TObjectIterator<UChessPiece> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() != Itr->GetTeam())
		{
			Itr->SendPossibleMovesToEnemyKing(1); // moveStage 1
		}
	}
}


void UChessBoard::SetNextPlayersCheckState()
{
	// Reset enemy's kings InCheck
	for (TObjectIterator<UChessKing> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() == Itr->GetTeam())
		{
			Itr->ResetIsInCheck();
		}
	}

	// check possible moves for own pieces against enemy king position to set InCheck
	for (TObjectIterator<UChessPiece> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() != Itr->GetTeam()) 
		{
			Itr->SendPossibleMovesToEnemyKing(2); // moveStage 2
		}
	}
}


void UChessBoard::SetNextPlayersCheckMateState()
{
	// Reset enemy's kings CheckMate
	for (TObjectIterator<UChessKing> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() == Itr->GetTeam())
		{
			Itr->ResetIsCheckMate(); // sets it to true
		}
	}

	// check possible moves for own pieces against enemy king position to set InCheck
	for (TObjectIterator<UChessPiece> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() == Itr->GetTeam())
		{
			Itr->SendPossibleMovesToEnemyKing(3); // moveStage 3
		}
	}	
}


void UChessBoard::EndOfGameCheck()
{
	// Check Mate display
	for (TObjectIterator<UChessKing> Itr; Itr; ++Itr)
	{
		//World Check 
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (GetWhosTurn() == Itr->GetTeam() && Itr->GetIsCheckMate()) // and Itr->GetIsCheckMate()
		{
			Turn = 0; // Disable Selection
			GameOver = true;

			//Will change through enum
			if (Itr->GetTeam() == 1) { Winner = 2; }
			if (Itr->GetTeam() == 2) { Winner = 1; }
		}
	}
}

// only used to display a UI message
void UChessBoard::SetWhoIsInCheck(int team)
{
	TeamInCheck = team;
}