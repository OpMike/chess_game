// Michael Opoku 2017

#pragma once

#include "Components/ActorComponent.h"
#include "ChessPiece.h"
#include "Selecter.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API USelecter : public UActorComponent
{
	GENERATED_BODY()

public:	
	USelecter();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* BoardActor = nullptr;

private:

	UPROPERTY(VisibleAnywhere)
	AActor* SelectedPiece = nullptr;
	
	UInputComponent* InputComponent = nullptr;
	APlayerController* PlayerController = nullptr;
	UChessPiece* Piece = nullptr;
	UChessBoard* Board = nullptr;
	
	void SetupInputComponent();
	void SetupChessBoard();

	void Select();
	void Deselect();

private:
	virtual void BeginPlay() override;
};