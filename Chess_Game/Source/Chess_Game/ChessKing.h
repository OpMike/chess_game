// Michael Opoku 2017

#pragma once

#include "ChessQueen.h"
#include "ChessKing.generated.h"

USTRUCT()
struct FPositionStruct
{
	GENERATED_BODY()

	UPROPERTY()
		FIntPoint Position;

	UPROPERTY()
		bool PositionInCheck;

	void SetPosition(FIntPoint Pos)
	{
		Position = Pos;
	}
	void SetPosition(int x, int y)
	{
		Position.X = x;
		Position.Y = y;
	}
	
	void SetPositionInCheck(bool InCheck)
	{
		PositionInCheck = InCheck;
	}

	FIntPoint GetPosition()
	{
		return Position;
	}

	bool GetPositionInCheck()
	{
		return PositionInCheck;
	}

	//Constructor
	FPositionStruct(int x, int y, bool InCheck)
	{
		PositionInCheck = InCheck;
		Position.X = x;
		Position.Y = y;
	}

	FPositionStruct()
	{
		PositionInCheck = false;
		Position.X = -1;
		Position.Y = -1;
	}
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API UChessKing : public UChessQueen
{
	GENERATED_BODY()

public:	
	UChessKing();
	virtual void SendPossibleMovesToEnemyKing(int moveStage) override;

	void ResetIsMoveIntoCheck();
	void ResetIsInCheck();
	void ResetIsCheckMate();

	void SetCheckMateFalse();
	void CheckIfMoveWouldLeaveKingInCheck(int x, int y);
	void CheckIfKingNowInCheck(int x, int y);

	bool GetIsMoveIntoCheck();
	bool GetIsCheckMate();

protected:
	virtual bool IsValidMove(int x, int y) override;
	bool IsMoveIntoCheck;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsInCheck;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsCheckMate;

private:
	virtual void BeginPlay() override;
};