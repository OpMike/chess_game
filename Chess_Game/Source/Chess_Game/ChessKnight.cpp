// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessKnight.h"


UChessKnight::UChessKnight()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessKnight::BeginPlay()
{
	Super::BeginPlay();	
}


bool UChessKnight::IsValidMove(int x, int y)
{
	bool MoveValid = true;
	UChessPiece* PieceAtXY = Board->FindPieceAt(x, y);
	int DiffX = std::abs(CurrentGridLocation.X - x);
	int DiffY = std::abs(CurrentGridLocation.Y - y);


	// check knight jump
	if (std::abs(DiffX - DiffY) == 1 && CurrentGridLocation.X != x && CurrentGridLocation.Y != y)
	{
		if (PieceAtXY != nullptr && PieceAtXY->GetTeam() == Team)
		{
			MoveValid = false;
		}
	}
	else {
		MoveValid = false;
	}

	// ? :
	if (TryToMoveTo(x, y) == false)
	{
		return false;
	}

	return MoveValid;
}


void UChessKnight::SendPossibleMovesToEnemyKing(int moveStage)
{
	TransmitPossibleMovesInDirToEnemyKing(2, 1, moveStage);
	TransmitPossibleMovesInDirToEnemyKing(2, -1, moveStage);

	TransmitPossibleMovesInDirToEnemyKing(1, 2, moveStage);
	TransmitPossibleMovesInDirToEnemyKing(-1, 2, moveStage);

	TransmitPossibleMovesInDirToEnemyKing(-2, 1, moveStage);
	TransmitPossibleMovesInDirToEnemyKing(-2, -1, moveStage);

	TransmitPossibleMovesInDirToEnemyKing(1, -2, moveStage);
	TransmitPossibleMovesInDirToEnemyKing(-1, -2, moveStage);
}