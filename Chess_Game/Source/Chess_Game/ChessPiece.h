// Michael Opoku 2017

#pragma once

#include "Components/ActorComponent.h"
#include "ChessBoard.h"
#include "ChessPiece.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API UChessPiece : public UActorComponent
{
	GENERATED_BODY()

public:	
	UChessPiece();
	virtual void MoveToGrid(int x, int y);
	virtual void CallDestroy();
	virtual void UpdateCurrentGridLocation();
	virtual void SendPossibleMovesToEnemyKing(int moveStage);
	
	FIntPoint* GetCurrentGridLocation();
	void SetCurrentGridLocation(FIntPoint);
	int GetTeam();
	bool TryToMoveTo(int x, int y);
	FString GetPieceName();

protected:
	virtual void BeginPlay() override;
	virtual bool IsValidMove(int x, int y);
	void TransmitPossibleMovesInDirToEnemyKing(int x, int y, int moveStage);

	UChessBoard* Board = nullptr;
	UChessPiece* EnemyKing;
	UChessPiece* MyKing;


	UPROPERTY(VisibleAnywhere)
	FIntPoint CurrentGridLocation = FIntPoint(-1, -1);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MoveLimit = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Team = 0; //TODO create enum
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString PieceName = "PlaceHolder";

private:
	void SetupChessBoard();
};
