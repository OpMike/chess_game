// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessBishop.h"
#include "ChessKing.h"


UChessBishop::UChessBishop()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessBishop::BeginPlay()
{
	Super::BeginPlay();	
}


bool UChessBishop::IsValidMove(int x, int y)
{
	bool MoveValid = true;
	UChessPiece* PieceAtXY = Board->FindPieceAt(x, y);
	int DiffX = std::abs(CurrentGridLocation.X - x);
	int DiffY = std::abs(CurrentGridLocation.Y - y);
	int DirX = 1;
	int DirY = 1;


	// check diagonal move
	if (DiffX == DiffY && DiffY <= MoveLimit && CurrentGridLocation.X != x && CurrentGridLocation.Y != y)
	{
		if (CurrentGridLocation.X > x) { DirX = -1; }
		if (CurrentGridLocation.Y > y) { DirY = -1; }

		// check if path to destination is obstructed by another piece
		for (int i = 1; i < DiffY; i++)
		{

			if (Board->FindPieceAt(CurrentGridLocation.X + (i * DirX), CurrentGridLocation.Y + (i * DirY)) != nullptr)
			{
				MoveValid = false;
				break;
			}
		}
		
		if (PieceAtXY != nullptr && PieceAtXY->GetTeam() == Team)
		{
			MoveValid = false;
		}

	}else{
		MoveValid = false;
	}

	if (TryToMoveTo(x, y) == false)
	{
		MoveValid = false;
	}

	return MoveValid;
}


void UChessBishop::SendPossibleMovesToEnemyKing(int moveStage)
{
	// Right Up Dir
	TransmitPossibleMovesInDirToEnemyKing(1, 1, moveStage);

	// Right Down Dir
	TransmitPossibleMovesInDirToEnemyKing(1, -1, moveStage);

	// Left Down Dir
	TransmitPossibleMovesInDirToEnemyKing(-1, -1, moveStage);

	// Left Up Dir
	TransmitPossibleMovesInDirToEnemyKing(-1, 1, moveStage);
}