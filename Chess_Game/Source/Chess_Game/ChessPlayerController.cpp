// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessPlayerController.h"

void AChessPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetupChessBoard();
}

void AChessPlayerController::SetupChessBoard()
{
	for (TObjectIterator<UChessBoard> Itr; Itr; ++Itr)
	{
		if (Itr->GetWorld() != GetWorld()) { continue; }

		Board = *Itr;
		break;
	}
}

UChessBoard* AChessPlayerController::GetChessBoard() const
{
	return Board;
}
