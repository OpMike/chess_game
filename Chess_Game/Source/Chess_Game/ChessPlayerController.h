// Michael Opoku 2017

#pragma once

#include "ChessBoard.h"
#include "GameFramework/PlayerController.h"
#include "ChessPlayerController.generated.h"


UCLASS()
class CHESS_GAME_API AChessPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	UFUNCTION(BlueprintCallable, Category = "Setup")
	UChessBoard* GetChessBoard() const;


private:
	virtual void BeginPlay() override;
	void SetupChessBoard();
	UChessBoard* Board = nullptr;
};
