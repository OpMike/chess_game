// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessRook.h"
#include "ChessKing.h"


UChessRook::UChessRook()
{
	PrimaryComponentTick.bCanEverTick = true;

}


void UChessRook::BeginPlay()
{
	Super::BeginPlay();
}

bool UChessRook::IsValidMove(int x, int y)
{
	bool MoveValid = true;
	UChessPiece* PieceAtXY = Board->FindPieceAt(x, y);

	int DiffX = std::abs(CurrentGridLocation.X - x);
	int DiffY = std::abs(CurrentGridLocation.Y - y);
	int Dir = 1;

	// limit rook movement to x/y direction only
	if (!(CurrentGridLocation.X == x && y != CurrentGridLocation.Y && DiffY <= MoveLimit) && !(CurrentGridLocation.Y == y && x != CurrentGridLocation.X && DiffX <= MoveLimit))
	{
		MoveValid = false;

	}else if (CurrentGridLocation.Y == y && x != CurrentGridLocation.X)
	{	
		if (x < CurrentGridLocation.X)
		{
			Dir = -1;
		}

		// check if path to destination is obstructed by another piece
		for (int i = 1; i < DiffX; i++)
		{	
			if (Board->FindPieceAt(CurrentGridLocation.X + (i * Dir), CurrentGridLocation.Y) != nullptr)
			{
				MoveValid = false;
				break;
			}
		}
				
		if (PieceAtXY != nullptr && PieceAtXY->GetTeam() == Team)
		{
			MoveValid = false;
		}

	}else if (CurrentGridLocation.X == x && y != CurrentGridLocation.Y)
	{
		if (y < CurrentGridLocation.Y)
		{
			Dir = -1;
		}

		// check if path to destination is obstructed by another piece
		for (int i = 1; i < DiffY; i++)
		{
			if (Board->FindPieceAt(CurrentGridLocation.X, CurrentGridLocation.Y + (i * Dir)) != nullptr )
			{
				MoveValid = false;
				break;
			}
		}

		// Check if target piece is friendly
		if (PieceAtXY != nullptr && PieceAtXY->GetTeam() == Team)
		{
			MoveValid = false;
		}
	}

	if (TryToMoveTo(x, y) == false)
	{
		MoveValid = false;
	}

	return MoveValid;
}



void UChessRook::SendPossibleMovesToEnemyKing(int moveStage)
{
	// X right Dir
	TransmitPossibleMovesInDirToEnemyKing(1, 0, moveStage);

	// X Left Dir
	TransmitPossibleMovesInDirToEnemyKing(-1, 0, moveStage);

	// Y up Dir
	TransmitPossibleMovesInDirToEnemyKing(0, 1, moveStage);

	// Y down Dir
	TransmitPossibleMovesInDirToEnemyKing(0, -1, moveStage);
}