// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessPawn.h"


UChessPawn::UChessPawn()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessPawn::BeginPlay()
{
	Super::BeginPlay();
}


bool UChessPawn::IsValidMove(int x, int y)
{
	bool MoveValid = true;
	UChessPiece* PieceAtXY = Board->FindPieceAt(x, y);
	int DiffX = std::abs(CurrentGridLocation.X - x);
	int DiffY = std::abs(CurrentGridLocation.Y - y);
	int Dir = 1;

	// check diagonal capture move
	if (DiffX == DiffY && DiffY <= MoveLimit && ( CurrentGridLocation.Y < y && Team == 1 || CurrentGridLocation.Y > y && Team == 2 ))
	{
		if (PieceAtXY == nullptr || PieceAtXY->GetTeam() == Team || DiffY > 1)
		{
			MoveValid = false;
		}

	}else if (CurrentGridLocation.X != x || DiffY > MoveLimit ||   CurrentGridLocation.Y > y && Team == 1  || CurrentGridLocation.Y < y && Team == 2) // limit direction and number of moves 
	{
		MoveValid = false;
	}
	else {
		if (Team == 2)
		{
			Dir = -1;
		}

		// check if path to destination is obstructed by another piece
		for (int i = 1; i <= DiffY; i++)
		{
			if (Board->FindPieceAt(CurrentGridLocation.X, CurrentGridLocation.Y + (i * Dir)) != nullptr)
			{
				MoveValid = false;
				break;
			}
		}
	}

	if (TryToMoveTo(x, y) == false)
	{
		return false;
	}

	// after first valid move set move limit back to 1;
	if (MoveValid && MoveLimit != 1)
	{
		MoveLimit = 1;
	}

	return MoveValid;
}


void UChessPawn::SendPossibleMovesToEnemyKing(int moveStage)
{
	if (GetTeam() == 1)
	{
		// Right Up Dir
		TransmitPossibleMovesInDirToEnemyKing(1, 1, moveStage);
		// Left Up Dir
		TransmitPossibleMovesInDirToEnemyKing(-1, 1, moveStage);
		if (moveStage == 3)
		{
			TransmitPossibleMovesInDirToEnemyKing(0, 1, moveStage);

		}
	}
	else
	{
		// Right Down Dir
		TransmitPossibleMovesInDirToEnemyKing(1, -1, moveStage);
		// Left Down Dir
		TransmitPossibleMovesInDirToEnemyKing(-1, -1, moveStage);

		if (moveStage == 3)
		{
			TransmitPossibleMovesInDirToEnemyKing(0, -1, moveStage);

		}
	}
}