// Michael Opoku 2017

#pragma once

#include "Components/ActorComponent.h"
#include "ChessBoard.generated.h"


class UChessPiece;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API UChessBoard : public UActorComponent
{
	GENERATED_BODY()

public:	
	UChessBoard();

	UChessPiece* FindPieceAt(int, int);	
	void DeletePieceAt(int, int);
	int GetWhosTurn();
	void NextTurn();
	void CalculateAllPossibleMoves();
	void SetNextPlayersCheckState();
	void SetNextPlayersCheckMateState(); 
	void EndOfGameCheck(); 
	void SetWhoIsInCheck(int team);


	// Team 1 (White) begins
	UPROPERTY(BlueprintReadOnly)
	int Turn = 1; // Will be replaced by enum

	UPROPERTY(BlueprintReadOnly)
	int Winner = 0;

	UPROPERTY(BlueprintReadOnly)
	bool GameOver = false;

	UPROPERTY(BlueprintReadOnly)
	int TeamInCheck = 0;

private:
	virtual void BeginPlay() override;
};