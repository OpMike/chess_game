// Michael Opoku 2017

#pragma once

#include "ChessPiece.h"
#include "ChessRook.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API UChessRook : public UChessPiece
{
	GENERATED_BODY()

public:	
	UChessRook();
	virtual void SendPossibleMovesToEnemyKing(int moveStage) override;


protected:
	virtual void BeginPlay() override;
	virtual bool IsValidMove(int x, int y) override;
};