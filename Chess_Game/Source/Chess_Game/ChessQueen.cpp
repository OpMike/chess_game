// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessQueen.h"


UChessQueen::UChessQueen()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessQueen::BeginPlay()
{
	Super::BeginPlay();
}


bool UChessQueen::IsValidMove(int x, int y)
{
	bool MoveValid = true;
	UChessPiece* PieceAtXY = Board->FindPieceAt(x, y);
	int DiffX = std::abs(CurrentGridLocation.X - x);
	int DiffY = std::abs(CurrentGridLocation.Y - y);
	int DirX = 1;
	int DirY = 1;


	// check diagonal move (Bishop)
	if (DiffX == DiffY && CurrentGridLocation.X != x && CurrentGridLocation.Y != y)
	{

		if (CurrentGridLocation.X > x) { DirX = -1; }
		if (CurrentGridLocation.Y > y) { DirY = -1; }

		// check if path to destination is obstructed by another piece
		for (int i = 1; i < DiffY; i++)
		{
			if (Board->FindPieceAt(CurrentGridLocation.X + (i * DirX), CurrentGridLocation.Y + (i * DirY)) != nullptr)
			{
				MoveValid = false;
				break;
			}
		}

		if (PieceAtXY != nullptr && PieceAtXY->GetTeam() == Team)
		{
			MoveValid = false;
		}

		if (TryToMoveTo(x, y) == false)
		{
			MoveValid = false;
		}
	}
	else {
		// Rook
		MoveValid = Super::IsValidMove(x, y);

	}

	return MoveValid;
}


void UChessQueen::SendPossibleMovesToEnemyKing(int moveStage)
{
	// Rook
	Super::SendPossibleMovesToEnemyKing(moveStage);

	// Bishop  
	// Right Up Dir
	TransmitPossibleMovesInDirToEnemyKing(1, 1, moveStage);

	// Right Down Dir
	TransmitPossibleMovesInDirToEnemyKing(1, -1, moveStage);

	// Left Down Dir
	TransmitPossibleMovesInDirToEnemyKing(-1, -1, moveStage);

	// Left Up Dir
	TransmitPossibleMovesInDirToEnemyKing(-1, 1, moveStage);
}