// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessPiece.h"
#include "ChessPawn.h"
#include "ChessKing.h"

#include <typeinfo> 
#include <cstdlib>


UChessPiece::UChessPiece()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessPiece::BeginPlay()
{
	Super::BeginPlay();
	UpdateCurrentGridLocation();
	SetupChessBoard();
}


void UChessPiece::MoveToGrid(int x, int y)
{
	if (IsValidMove(x, y))
	{
		if (Board != nullptr)
		{
			Board->DeletePieceAt(x, y);

			GetOwner()->SetActorLocation(FVector(y * 400, x * 400, 1.f) + FVector(200.f, 200.f, 1.f));
			UpdateCurrentGridLocation(); // TODO replace with SetCurrentGridLocation(x,y) ?

			Board->NextTurn();
			Board->SetNextPlayersCheckState(); 
			Board->SetNextPlayersCheckMateState();
			Board->EndOfGameCheck();
		}
	}
	else {
		if (GEngine != nullptr)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, FString::Printf(TEXT("Invalid move! Try again.")));
		}
		UE_LOG(LogTemp, Log, TEXT("Invalid move. Try again."));

	}	
}


// move piece to target square and check if your king would be in check, if it is move it back
bool UChessPiece::TryToMoveTo(int x, int y)
{
	// if I would be at target xy still in check?:
	int _x = CurrentGridLocation.X;
	int _y = CurrentGridLocation.Y;

	// temporarily remove target piece if present
	int targetPreviousX = 0;
	int targetPreviousY = 0;

	UChessPiece* TargetPiece = Board->FindPieceAt(x, y);

	// Pawn specific
	if (TargetPiece == nullptr && this->IsA(UChessPawn::StaticClass()) && CurrentGridLocation.X != x)
	{
		return false;
	}

	if (TargetPiece && TargetPiece->GetTeam() == GetTeam())
	{
		return false;
	}

	if (TargetPiece)
	{
		targetPreviousX = TargetPiece->GetCurrentGridLocation()->X;
		targetPreviousY = TargetPiece->GetCurrentGridLocation()->Y;

		// temporarily move enemy piece at target xy off the board
		TargetPiece->SetCurrentGridLocation(FIntPoint(-20,-20));
	}

	// temporarily move piece to new xy
	SetCurrentGridLocation(FIntPoint(x, y));

	// Tell all enemy pieces to send their possible moves (cordinates) to find out if the king would be in check
	Board->CalculateAllPossibleMoves();

	// move piece back
	SetCurrentGridLocation(FIntPoint(_x, _y));
	
	if (TargetPiece)
	{
		TargetPiece->SetCurrentGridLocation(FIntPoint(targetPreviousX, targetPreviousY));
	}
	
	// TODO simplify with ? :
	if (((UChessKing*)MyKing)->GetIsMoveIntoCheck())
	{	
		return false;
	}
	else
	{
		return true;
	}
}


// called first on enemy pieces to validate the move, then on own pieces to set InCheck on the enemy king and again after the move to set IsCheckMate state on the enemy king
void UChessPiece::TransmitPossibleMovesInDirToEnemyKing(int dirX, int dirY, int moveStage) // has 3 stages
{
	int x, y = 0;

	for (int i = 1; i <= MoveLimit; i++)
	{
		x = CurrentGridLocation.X + (i * dirX);
		y = CurrentGridLocation.Y + (i * dirY);

		// Board out of bounds
		if (x > 7 || x < 0 || y > 7 || y < 0 )
		{
			break;
		}

		// Checks the current possible move against the enemy kings position

		if (moveStage == 1)
		{	
			((UChessKing*)EnemyKing)->CheckIfMoveWouldLeaveKingInCheck(x, y);
		}
		else if (moveStage == 2)
		{	
			((UChessKing*)EnemyKing)->CheckIfKingNowInCheck(x, y);
		}
		else if (moveStage == 3)
		{	
			if (TryToMoveTo(x, y))
			{
				((UChessKing*)MyKing)->SetCheckMateFalse();
				break; // no need to look for more moves
			}
		}
		
		if (Board->FindPieceAt(x, y) != nullptr)
		{
			break;
		}

		// workaround the pawns first move 
		if (this->IsA(UChessPawn::StaticClass()))
		{
			return;
		}
	}
}


void UChessPiece::SetupChessBoard()
{
	for (TObjectIterator<UChessBoard> Itr; Itr; ++Itr)
	{
		//World Check 
		if (Itr->GetWorld() != GetWorld()) { continue; }

		Board = *Itr;
		break;
	}

	if (Board == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Attach board to %s."), *GetOwner()->GetName());
	}

	// set enemy/own king
	for (TObjectIterator<UChessKing> Itr; Itr; ++Itr)
	{
		//World Check 
		if (Itr->GetWorld() != GetWorld()) { continue; }

		if (Itr->GetTeam() != Team) { EnemyKing = *Itr; }

		if (Itr->GetTeam() == Team) { MyKing = *Itr; }
	}

	if (MyKing == nullptr || EnemyKing == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Kings not set!"));
	}
}


void UChessPiece::UpdateCurrentGridLocation()
{
	FVector loc = GetOwner()->GetActorLocation() / FVector(400.f, 400.f, 1.f);

	CurrentGridLocation.Y = (int)loc.X;
	CurrentGridLocation.X = (int)loc.Y;
}


void UChessPiece::SendPossibleMovesToEnemyKing(int moveStage)
{
	//	ie	TransmitPossibleMovesInDirToEnemyKing(1, 1, moveDone);
}


FIntPoint* UChessPiece::GetCurrentGridLocation()
{
	return &CurrentGridLocation;
}


void UChessPiece::SetCurrentGridLocation(FIntPoint xy)
{
	CurrentGridLocation = xy;
}


void UChessPiece::CallDestroy()
{
	GetOwner()->Destroy();
	UE_LOG(LogTemp, Log, TEXT("CallDestroy() called on %s"), *GetOwner()->GetName());
}


bool UChessPiece::IsValidMove(int x, int y)
{
	return false;
}


FString UChessPiece::GetPieceName()
{
	return PieceName;
}


int UChessPiece::GetTeam()
{
	return Team;
}