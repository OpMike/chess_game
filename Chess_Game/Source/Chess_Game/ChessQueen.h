// Michael Opoku 2017

#pragma once

#include "ChessRook.h"
#include "ChessQueen.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_GAME_API UChessQueen : public UChessRook
{
	GENERATED_BODY()

public:	
	UChessQueen();
	virtual void SendPossibleMovesToEnemyKing(int moveStage) override;

protected:
	virtual void BeginPlay() override;
	virtual bool IsValidMove(int x, int y) override;
};