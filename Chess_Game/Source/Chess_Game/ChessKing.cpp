// Michael Opoku 2017

#include "Chess_Game.h"
#include "ChessKing.h"


UChessKing::UChessKing()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UChessKing::BeginPlay()
{
	Super::BeginPlay();
	ResetIsMoveIntoCheck();
	ResetIsInCheck();
	ResetIsCheckMate();
}


void UChessKing::ResetIsInCheck()
{	
	Board->SetWhoIsInCheck(0);
	IsInCheck = false;
}


void UChessKing::ResetIsCheckMate()
{
	IsCheckMate = true;
}


void UChessKing::ResetIsMoveIntoCheck()
{
	IsMoveIntoCheck = false;
}


void UChessKing::CheckIfMoveWouldLeaveKingInCheck(int x, int y)
{
	if (GetCurrentGridLocation()->X == x && GetCurrentGridLocation()->Y == y)
	{
		IsMoveIntoCheck = true;
	}
}


void UChessKing::CheckIfKingNowInCheck(int x, int y)
{
	if (GetCurrentGridLocation()->X == x && GetCurrentGridLocation()->Y == y)
	{
		Board->SetWhoIsInCheck(Team);
		IsInCheck = true;
	}
}


bool UChessKing::IsValidMove(int x, int y)
{
	return Super::IsValidMove(x, y);
}


void UChessKing::SendPossibleMovesToEnemyKing(int moveStage)
{
	Super::SendPossibleMovesToEnemyKing(moveStage);
}


bool UChessKing::GetIsMoveIntoCheck()
{
	return IsMoveIntoCheck;
}


void UChessKing::SetCheckMateFalse()
{
	IsCheckMate = false;
}


bool UChessKing::GetIsCheckMate()
{
	return IsCheckMate;
}